# finesse.ifosim.org

## Theme code

The theme code is forked from the Scientific Python Theme source code [Scientific Python Theme source code](https://github.com/scientific-python/scientific-python-hugo-theme). It is currently hosted [in the finesse group](https://gitlab.com/ifosim/finesse/scientific-python-hugo-theme).

If you make changes to the theme, first commit & push the changes in the theme repo (ensure you are working on the `finesse3` branch), then commit and push the theme folder in the website repo. Finally run `git submodule update --remote` in the website repo to update which commit the website references.

## View page locally

Install Hugo: <https://gohugo.io/getting-started/installing/>

Clone the repository, `cd` into it and run: `git submodule update --init --recursive`

Run `make serve` and it should start a server accessible at `http://localhost:1313`

## View page online

You can view a preview of the `develop` branch current site in the repo here: <https://ifosim.gitlab.io/finesse/finesse.ifosim.org/>

For other branches, artifacts are being kept which you can view by selecting:

[pipeline](https://gitlab.com/ifosim/finesse/finesse.ifosim.org/-/pipelines) -> Stages -> test -> Job artifacts -> Browse -> public -> index.html

and finally clicking the blue link.
****
