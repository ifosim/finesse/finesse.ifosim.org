---
title: New Finesse 3 Website
authors: ["Anna Green"]
date: 2023-01-31
summary: Welcome to the new Finesse 3 website!
---

Welcome to the new Finesse 3 website! 
We're gradually populating this as we get closer to the beta and full launches of Finesse 3.0 in the upcoming year. 
Explore around to find our documentation and other information about the software, and check back for updates.
Questions? Suggestions? Contact the [team](https://finesse.ifosim.org/docs/latest/backmatter/about.html)
<!-- nb add new news items as new files. it seems these get auto-sorted by date, and are automatically hidden if that date is in the future :)-->
