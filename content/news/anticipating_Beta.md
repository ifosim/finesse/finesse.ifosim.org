---
title: Finesse 3 approaches Beta release
authors: ["Anna Green"]
date: 2023-12-25
summary: We're currently targeting releasing Finesse 3 Beta during 2023.
---

This is an example news item. 
<!-- nb add new news items as new files. it seems these get auto-sorted by date, and are automatically hidden if that date is in the future :)-->
